Configuración Global
git config --global user.name "<nombre>": Establece el nombre de usuario global de Git.
git config --global user.email "<correo>": Establece la dirección de correo electrónico global de Git.

Comandos Básicos
git init: Crea un nuevo repositorio Git vacío.
git clone <url>: Clona un repositorio remoto en la ubicación local.
git add <archivo>: Agrega el archivo al área de preparación (staging).
git commit -m "<mensaje>": Crea un nuevo commit con los cambios agregados al área de preparación y un mensaje descriptivo.
git status: Muestra el estado actual del repositorio (archivos modificados, archivos agregados, etc.).
git log: Muestra un historial de commits en el repositorio.