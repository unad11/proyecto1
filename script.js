var nombres = [
	"Jairo"
	"victor",
	"Adriana",
	"Mariano",
	"Ulises",
	//"Juan",
	"LUIS",
	"Melissa",
	"Yan Carlos",
	//tu_nombre_aqui,

	"THOMAS PACHECO------------->>>>>aqui<______"
];

var firmasMostradas = [];

function obtenerFirmaAleatoria() {
	// Si se han mostrado todas las firmas, regresa null
	if (firmasMostradas.length === nombres.length) {
		return null;
	}

	// Obtiene un nombre aleatorio que no se ha mostrado antes
	var firma = null;
	while (firma === null) {
		var indice = Math.floor(Math.random() * nombres.length);
		var nombre = nombres[indice];
		if (firmasMostradas.indexOf(nombre) === -1) {
			firma = nombre;
			firmasMostradas.push(nombre);
		}
	}
	return firma;
}

function agregarFirma() {
	var firma = obtenerFirmaAleatoria();
	if (firma !== null) {
		var muro = document.getElementById("muro");
		var firmaDiv = document.createElement("div");
		firmaDiv.innerHTML = firma;
		firmaDiv.classList.add("firma");
        firmaDiv.style.left = Math.max(Math.random() * (muro.offsetWidth - firmaDiv.offsetWidth), 0) + "px";
        firmaDiv.style.top = Math.max(Math.random() * (muro.offsetHeight - firmaDiv.offsetHeight), 0) + "px";
		muro.appendChild(firmaDiv);
	} else {
		clearInterval(intervalo);
	}
}

var intervalo = setInterval(agregarFirma, 1500);
<script>document.write("hola_mundo");</script>